<p>Bonjour,</p>

<p>Suite à la réception de l'appel à expression d’intérêt concernant la nouvelle identité visuelle pour iMAL, nous vous faisons parvenir cette note d'intention, suivie d'un portfolio.</p>

<p>Nous sommes Luuse, un groupe de 6 designers graphiques et développeurs, basé à Bruxelles.</p>

<p>Par sa pratique, Luuse, s'inscrit dans une recherche constante, qui place le dialogue entre le designer et son environnement au centre de ses préoccupations. 
Il y a dans notre manière d’aborder tout projet, une certitude: les méthodes et outils mis en place ont leur importance. La technique n'est pas un mal nécessaire, mais un bien porteur de nouvelles pistes de recherches formelles et théoriques.
Plutôt que de vouloir la prouesse technique, nous cherchons ses singularités, par la transgression d'usages et le développement de méthodes combinatoires entre différents objets ouverts. Luuse pense la technique comme matière réflexive, indispensable aux transmissions des savoirs et constitutions de communs.
Nous souhaitons une utilisation réfléchie, curieuse et consciente de nos outils tant dans la conception que dans le développement de nos projets.</p>

<p>Par ces conditions, Luuse tente de redéfinir son rôle de designer sous de multiples rapports. Notamment la manière d'inclure le public et les personnes avec qui nous travaillons au cœur des processus de travail. Notre démarche nous place dans une position d'auteur-relais que nous tentons de faire émerger au gré de nos commandes ou de notre travail de recherche. Cette posture de travail demande de reconsidérer la question de l’auteur au-delà de l’individu et de son propre intérêt.</p>

<p>Nous voyons dans cette potentielle collaboration entre l'iMAL et Luuse, un moment pour repenser, et requestionner un système d'écriture et d'identification. Qu'il soit déployé au sein même de la structure de l'iMAL ou bien dans l'espace public, nous pensons ce système comme évolutif et perfectible. Un système ouvert et intelligible, qui pourrait engager une participation dépassant notre propre rôle de designer. Nous tenterions au plus, par les outils et principes mis en place, de faire accueil à l'étude et encourager les action du public dans les diverses activités de l'iMAL.</p>

<p>Si nous vous soumettons une candidature pour repenser la nouvelle identité visuelle d'iMAL c'est parce que nous sommes sensibles aux problématiques que votre structure entend promouvoir. En effet l'iMAL est un centre des cultures numériques et technologiques, qui au travers de ses diverses activités, met en valeur leur complexité et leur intérêt. Cette attention portée aux environnements numériques fait partie intégrante de notre pratique. Nous sommes conscients, à l'instar d'iMAL, que le numérique qui prend place et s’accroît dans nos vies, porte en lui de nombreuses questions: politiques, sociales, artistiques ou techniques. </p>

<p>Nous souhaitons vivement  pouvoir entamer une collaboration et nous espérons en ce sens avoir l'opportunité de définir ensemble ces futurs principes de création.</p>

<p>bien à vous,
Luuse</p>
