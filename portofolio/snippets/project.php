<?php if(!$project->hasSubprojects && $project->images): ?>
	<div class="slider <?= (count($project->images) > 1) ? 'multiple' : '' ?>">
		<?php foreach($project->images as $num=>$image): ?>
			<img src="<?= ($num < 2) ? $image : '' ?>" class="<?= ($num) !== 0 ? 'hidden' : 'visible' ?> <?= $images->isGif($image) ? 'gif' : '' ?> <?= $images->orientation($image) ?>" data-num="<?= $num+1 ?>" data-src="<?= $image ?>">
		<?php endforeach ?>
	</div>
<?php endif ?>
<div class="legende <?= ($project->hasSubprojects) ? 'title' : '' ?>">
	<?php if(!$project->hasSubprojects): ?>
		<h2>
			<?= ($project->parent) ? '<span>' . $project->parent.'</span></br>' : '' ?>
			<?= $project->title ?>
		</h2>
	<?php endif ?>
	<?php if($project->year || $project->type  || $project->language): ?>
		<ul>
			<?= ($project->type) ? "<li class='type'>".$project->type."</li>" : "" ?>
			<?= ($project->language) ? "<li class='language'>".$project->language."</li>" : "" ?>
			<?= ($project->year) ? "<li class='year'>".$project->year."</li>" : "" ?>
		</ul>
	<?php endif ?>
	<?= ($project->text) ? "<p>".$project->text."</p>" : "" ?>
	<?php if(!is_array($strings->strToTags($project->url))): ?>
		<a target="blank" href="<?= $project->url ?>"><?= $strings->showUrl($project->url) ?></a>
		<?php else: ?>
			<?php foreach ($strings->strToTags($project->url) as $url): ?>
				<a target="blank" href="<?= $url ?>"><?= $strings->showUrl($url)  ?></a>
			<?php endforeach ?>
		<?php endif ?>
		<?php if(count($project->images) > 1): ?>
			<div class="counter">
				<span class="prev"><</span>
				<span class="num"><span class="current">1</span> / <span class="total"><?= count($project->images) ?></span></span>
				<span class="next">></span>
			</div>
		<?php endif ?>
	</div>
