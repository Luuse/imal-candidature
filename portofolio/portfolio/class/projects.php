<?php

class Projects extends Strings{


	public function isNotDirFile($file){

		$dirFiles = [".","..",".DS_Store"];

		return (!in_array($file, $dirFiles)) ? true : false;
	}


	public function getUid($file){

		$str = substr($file, strrpos($file, "/")+1);

		return strtolower(substr($str, strpos($str, "-")+1));
	}


	public function renameImages($images, $project){

		foreach($images as $num => $image){

			$name = pathinfo($image, PATHINFO_FILENAME);
			$rename = $this->twoFig($num+1);
			$dir = pathinfo($image, PATHINFO_DIRNAME);
			$ext = pathinfo($image, PATHINFO_EXTENSION);

			if($name !== $rename){

				rename($image, $dir."/".$rename.".".$ext);
			}

		}

		return glob($project."/*.{jpg,png,gif,tif,tiff,JPG}", GLOB_BRACE);

	}


	public function load(){

		$projects = glob("content/projects/*");
		$backs = [];

		foreach ($projects as $project) {

			$back = [];

			if($this->isNotDirFile($project)){

				$infos = json_decode(file_get_contents($project."/project.json"));

				$subprojects = glob($project."/*", GLOB_ONLYDIR);

				$back = $infos;
				$back->uid = $this->getUid($project);
				$back->dir = $project;


				if(!$subprojects){

					$back->images = $this->renameImages(glob($project."/*.{jpg,png,gif,tif,tiff,JPG}", GLOB_BRACE), $project);
					$back->hasSubprojects = false;

				}else{

					$back->hasSubprojects = true;
					$back->subprojects = [];

					foreach ($subprojects as $key=>$subproject) {

						$subinfos = json_decode(file_get_contents($subproject."/project.json"));

						$back->subprojects[$key] = $subinfos;
						$back->subprojects[$key]->dir = $subproject;
						$back->subprojects[$key]->uid = $this->getUid($subproject);
						$back->subprojects[$key]->images = glob($subproject."/*.{jpg,png,gif,tif,tiff,JPG}", GLOB_BRACE);
						$back->subprojects[$key]->parent = $infos->title;

						sort($back->subprojects[$key]->images, SORT_STRING);

					}

				}

				$backs [] = $back;

			}

		}

		return $backs;

	}



}


?>
