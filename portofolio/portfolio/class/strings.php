<?php

class Strings{

	public function clean($str) {

		$string = str_replace(' ', '-', $str); 
		return strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $str)); 
	}

	public function strToTags($str){

		if(strpos($str, ",") > 0){

			$tags = explode(",", $str);

			foreach($tags as $key=>$tag){

				$tags [$key] = trim($tag);
			}

			return $tags;

		}else{

			return $str;
		}

	}

	public function twoFig($fig){

		if(strlen($fig) == 1){

			return "0".$fig;


		}else{

			return $fig;
		}
	}

	public function showUrl($url){

		$begins = ["http://www.", "https://www.", "http://", "https://"];
		$back = [];

		foreach($begins as $begin){

			$sample = substr($url, 0, strlen($begin));

			if($sample == $begin){

				if(substr($url, strlen($url)-1, strlen($url)-2) == "/"){

					$back []  = substr($url, strlen($begin), strlen($url)-strlen($begin)-1);

				}else{

					$back []  = substr($url, strlen($begin));
				}


			}
		}

		if(count($back) > 0){

			return (strpos($back[0], "/") > 0) ? substr($back[0], 0, strpos($back[0], "/")) : $back[0];

		}


	}
	
}

?>