<?php

class Images extends Projects{

	public function getResizeDimensions($max, $w, $h){

		return ($w > $h) ? [$max, round($max * ($h/$w))] : [round($max * ($w/$h)), $max];

	}

	public function resize($path, $max, $width, $height){

		$dimensions = $this->getResizeDimensions($max, $width, $height);
		$imagick = new Imagick(realpath($path));
		$imagick->resizeImage($dimensions[0], $dimensions[1], imagick::FILTER_UNDEFINED, 1);

		unlink($path);
		file_put_contents($path, $imagick->getImageBlob());

	}


	public function isImage($file){

		$imagesFiles = ["jpg","png","tif","tiff","JPG"];
		$extension = pathinfo($file, PATHINFO_EXTENSION);

		return (in_array($extension, $imagesFiles)) ? true : false;
	}


	public function getImages($path){

		$files = glob($path."/*");
		$hasFiles = count($files);
		$array = [];

		if($hasFiles){

			foreach($files as $file){

				$next = $this->getImages($file);

				if($this->isImage($file)){

					$array [] = $file;
				}

				if(isset($next)){

					foreach($next as $subfile){

						if($this->isImage($subfile)){

							$array [] = $subfile;
						}

					}

				}

			}

			return $array;
		}

	}

	public function checkImages($max){

		$images = $this->getImages("content/projects");
		set_time_limit(0);

		foreach($images as $image){

			list($width, $height) = getimagesize($image);

			if($width > $max || $height > $max){

				$this->resize($image, $max, $width, $height);
			}

		}

	}

	public function isGif($img){

		return (strtolower(pathinfo($img, PATHINFO_EXTENSION) == "gif")) ? true : false;
	}

	public function orientation($img){

		list($width, $height) = getimagesize($img);

		return ($width > $height) ? "landscape" : "portrait";
	}

}


?>
