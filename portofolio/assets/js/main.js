function setImage(image, num, nextImage, current){

	image.setAttribute("src", image.getAttribute("data-src"));
	nextImage.setAttribute("src", nextImage.getAttribute("data-src"));
	image.classList.remove("hidden");
	image.classList.add("visible");
	current.innerHTML=num;

}


function unsetImage(image){

	image.classList.add("hidden");
	image.classList.remove("visible");
	
}

function lazyLoad(image, slider, dir){

	var current = image.parentElement.parentElement.querySelector(".current");

	var num = parseInt(current.innerHTML)+dir;
	var total = parseInt(slider.parentElement.querySelector(".total").innerHTML);

	console.log(slider);

	var targetImg = slider.querySelector("img[data-num='"+num+"']");

	var altNum = (dir == 1) ? 1 : total;
	var altImg = slider.querySelector("img[data-num='"+altNum+"']");

	var thisImage = (targetImg) ? targetImg : altImg;
	var thisNum = (targetImg) ? num : altNum;

	var next = thisNum+dir;
	var nextImage = (slider.querySelector("img[data-num='"+next+"']")) ? slider.querySelector("img[data-num='"+next+"']") : slider.querySelector("img[data-num='"+altNum+"']");

	setImage(thisImage, thisNum, nextImage, current);
	unsetImage(image);
}


function slider() {
	
	var sliders = document.querySelectorAll(".slider");

	for(i=0; i<sliders.length; i++){

		var slides = sliders[i].getElementsByTagName("img");

		for(j=0; j<slides.length; j++){

			slides[j].addEventListener("click", function(){

				dir = 1;

				lazyLoad(this, this.parentElement, dir);

			});
		}
	}
}


function setArrow(arrow, dir){

	if(arrow){

		arrow.addEventListener("click", function(){

			var slider = this.parentElement.parentElement.parentElement.querySelector(".slider");		
			var currentImg = slider.querySelector("img.visible");

			lazyLoad(currentImg, slider, dir);

		});

	}
}

function setCounter(){

	var counters = document.querySelectorAll(".counter");

	for(i=0; i<counters.length; i++){

		var prev = counters[i].querySelector(".prev");
		var next = counters[i].querySelector(".next");

		setArrow(prev, -1);
		setArrow(next, 1);

	}
}

setCounter();
slider();