<?php include("snippets/header.php") ?>
<header>
	<h1><span>From → Luuse</span> <span>to → iMAL</span> </h1>
	<ul>
		<li>Luuse</li>
		<li>Rue de la Glacière 16</li>
		<li>1060 Saint Gilles</li>
		<li><a href="mailto:contact@luuse.io">contact@luuse.io</a> </li>
		<li>+32 489 38 63 08</li>
	</ul>
	<nav>
		<a href="#txt">Note d'intention</a>
		<br/>
		<?php foreach($projects->load() as $project): ?>
			<a href="#<?= $project->uid ?>"><?= $project->title ?></a>
			<?php if($project->hasSubprojects): ?>
				<div class="subprojects">
					<?php foreach($project->subprojects as $subproject): ?>
						<a href="#<?= $subproject->uid ?>"><?= $subproject->title ?></a>
					<?php endforeach ?>
				</div>
			<?php endif ?>
		<?php endforeach ?>
	</nav>
</header>
<main>
	<section id="txt">
		<?php include('snippets/text.php') ?>
	</section>
	<div class="break">	</div>
	<?php foreach($projects->load() as $project): ?>
		<section class="project" id="<?= $project->uid ?>">
			<?php include("snippets/project.php") ?>
			<?php if($project->hasSubprojects): ?>
				<?php foreach($project->subprojects as $subproject): ?>
					<?php $project = $subproject ?>
					<section class="subproject" id="<?= $project->uid ?>">
						<?php include("snippets/project.php") ?>
					</section>
				<?php endforeach ?>
			<?php endif ?>
		</section>
	<?php endforeach ?>
</main>
<?php include("snippets/footer.php") ?>
